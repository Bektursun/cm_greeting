<?php
defined('PHPFOX') or exit('NO DICE!');
?>

<div class="table_header">
    {_p('List')}
</div>
<form method="post" action="{url link='greeting.admincp.delete-greeting'}">
<table>
    <tr>
        <th style="width:20px;"></th>
        <th class="t_center">{_p('Name')}</th>
        <th class="t_center">{_p('Time')}</th>
        <th class="t_cneter">{_p('Image')}</th>
        <th class="t_center" style="width: 60px">{_p var='Active'}</th>

    </tr>
    {foreach from=$aGreets item=aGreet}
    <tr>
    <td class="t_center">
        <a href="#" class="js_drop_down_link" title="Manage">{img theme='misc/bullet_arrow_down.png' alt=''}</a>
        <div class="link_menu">
            <ul>
        <li><a class="popup" href="{url link='greeting.admincp.add-greeting' id=$aGreet.id}">{_p var='Edit'}</a></li>
        <li><a class="sJsConfirm" href="{url link='admincp/delete-greeting' delete=$aGreet.id}" >{_p var='Delete'}</a></li>
            </ul>
        </div>
    </td>

    <td class="t_center">{_p var=($aGreet.text)}</td>
        <td class="t_center">{$aGreet.start} - {$aGreet.end}</td>
        <td class="t_center">
            <input type="file" name="file">
        </td>
        <td class="t_center">
            <div class="js_item_is_active"{if !$aGreet.is_active} style="display:none;"{/if}>
            <a href="#?call=greeting.setStatus&amp;id={$aGreet.id}&amp;active=0" class="js_item_active_link" title="{_p var='Deactivate'}">{img theme='misc/bullet_green.png' alt=''}</a>
            </div>
            <div class="js_item_is_not_active"{if $aGreet.is_active} style="display:none;"{/if}>
            <a href="#?call=greeting.setStatus&amp;id={$aGreet.id}&amp;active=1" class="js_item_active_link" title="{_p var='Activate'}">{img theme='misc/bullet_red.png' alt=''}</a>
            </div>
        </td>
    </tr>
    {/foreach}
</table>
</form>
