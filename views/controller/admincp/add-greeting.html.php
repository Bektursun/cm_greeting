<?php
defined('PHPFOX') or exit('NO DICE!');
?>
    {if $bIsEdit}
    <form method="post" action="{url link='greeting.admincp.add-greeting' id=$aForms.id}">
        {else}
        <form method="post" action="{url link='greeting.admincp.add-greeting'}">
    {/if}
    <div class="form-group">
        {$aForms|var_dump}
        <label class="control-label">Start Time</label>
        <input type="time" name="val[start]" class="form-control" value="{value id='start' type='input'}" />
        <label class="control-label">End Time</label>
        <input type="time" name="val[end]" class="form-control" value="{value id='end' type='input'}" />
    </div>
    <div class="form-group">
    {foreach from=$aLanguages item=aLang}
        {assign var='value_name' value="name_"$aLang.language_id}
    <div>
        <strong>{_p('Text')}({$aLang.language_code})</strong>:
        <input type="text" name="val[text][{$aLang.language_id}]" value="{value id=$value_name type='input'}" size="30" />
    </div>
     {/foreach}
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary" name="_submit">Save</button>
    </div>
</form>
