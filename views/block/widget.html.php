<?php
defined('PHPFOX') or exit('NO DICE!');
?>
{*<div class="block" id="greeting" style="background: white">
        <div align="right">
        <a onclick="removeGreeting()">
            <i class="fa fa-times fa-2x" aria-hidden="true"></i>
        </a>
    </div>
    <div class="title" style="text-transform: capitalize;"><h3 align="center">{_p var=($aVal.text)} {$userName}</h3>
    </div>
    <div class="" align="center" style="background-color: #FFEB3B; height: 80px;">
        <i class="fa fa-sun-o fa-4x" aria-hidden="true" style="position: relative; top: 14px; background: yellow" ></i>
    </div>
</div>*}

<div class="newclass" style="
  margin-left: auto;
  margin-right: auto;
  padding: 2px;
  background-color: #f2f2f2;
  color: #444;
  font-weight: 300;
  line-height: 1;
  text-align: center;">


<i class="fa fa-sun-o fa-4x" aria-hidden="true" style=" position: relative; top: 14px;"></i>
<div
  style="margin-top: -3.5em;
  background-color: white;
  padding: 5em 1.5em 1.5em 1.5em;
  border-radius: 3px;
  box-shadow: 0 1px 2px rgba(0,0,0,0.1);">

    <h1
  style="margin-right: -1em;
  margin-bottom: .75em;
  margin-left: -1em;
  border-bottom: 1px solid rgba(0,0,0,0.1);
  padding-bottom: .75em;
  font-size: 1.5em;">{_p var=($aVal.text)}</h1>
    <span class="profile-title" style="color: #ccc; text-transform: capitalize;">{_p var=($userName)}</span>
</div>
</div>

{literal}
<script>
    function removeGreeting() {
        var elem = document.getElementById('greeting');
        elem.parentNode.removeChild(elem);
    }
</script>
{/literal}