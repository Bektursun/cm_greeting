<?php
            namespace Apps\CM_Greeting;

            use Core\App;

            /**
             * Class Install
             * @author  Neil
             * @version 4.5.0
             * @package Apps\CM_Greeting
             */
            class Install extends App\App
            {
                private $_app_phrases = [
        
                ];
                protected function setId()
                {
                    $this->id = 'CM_Greeting';
                }
                protected function setAlias() {
                    $this->alias = 'greeting';
            }
            protected function setName()
            {
                $this->name = 'CM_Greeting';
            }
            protected function setVersion() {
                    $this->version = '1.0.0';
            }
            protected function setSupportVersion() {
            $this->start_support_version = '4.5.0';
            $this->end_support_version = '4.5.0';
        }

        protected function setSettings() {}
        protected function setUserGroupSettings() {}
        protected function setComponent() {
            $this->admincp_menu = 'Menu Admin';
            $this->component =
                [
                    "block" =>
                        [
                            "widget" => ""
                        ],
                ];
        }
        protected function setComponentBlock()
        {
            $this->component_block =
                [
                    "Greeting Widget"        => [
                        "type_id"      => "0",
                        "m_connection" => "core.index-member",
                        "component"    => "widget",
                        "location"     => "7",
                        "is_active"    => "1",
                    ],
                ];
        }
        protected function setPhrase() {
            $this->phrase = $this->_app_phrases;
        }


        protected function setOthers() {

                   $this->admincp_action_menu = [
                       "greeting/admincp/add-greeting" => "Add Greeting"
                   ];
                   $this->admincp_route = '/greeting/admincp';
                   $this->admincp_menu = [
                       "List all" => "#"
                   ];

                   $this->database = [
                       'Apps\CM_Greeting\Installation\Database\CreateTable'
                   ];
            $this->_publisher = 'CodeMake It Company';
            $this->_publisher_url = 'https://codemake.org/';
            }

            }
