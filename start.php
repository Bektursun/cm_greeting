<?php

    use Apps\CM_Greeting\Controller\Admin\AddController;
    use Apps\CM_Greeting\Controller\Admin\ListGreetingController;
   event('app_settings', function ($settings){
    if (isset($settings['cm_greeting_enabled'])) {
        Admincp_Service_Module_Process::instance()->updateActivity('CM_Greeting', $settings['cm_greeting_enabled']);
    }
    });


$module = \Phpfox_Module::instance();

    $module
        ->addAliasNames('greeting', 'CM_Greeting')
        ->addComponentNames('controller',[
        'greeting.admincp.list' => ListGreetingController::class,
        'greeting.admincp.add-greeting' => AddController::class
    ])->addComponentNames('ajax', [
        'greeting.ajax' => '\Apps\CM_Greeting\Ajax\Ajax',
    ]);

    $module->addTemplateDirs([
       'greeting' => PHPFOX_DIR_SITE_APPS . 'CM_Greeting/views',
    ]);

    $module->addServiceNames([
        'greeting.browse'  => Apps\CM_Greeting\Service\Browse::class,
        'greeting.process' => Apps\CM_Greeting\Service\Process::class,
    ]);

    \Phpfox_Module::instance()->addComponentNames('block',
        [
            'greeting.widget' => 'Apps\CM_Greeting\Block\GreetingBlock',
        ])->addComponentNames('controller',
        [
            'greeting.widget' => 'Apps\CM_Greeting\Controller\GreetingController',
        ])->addComponentNames('ajax',
        [
          'greeting.ajax' => '\Apps\CM_Greeting\Ajax\Ajax',
        ]);


    group('/greeting', function (){
        route('/admincp', function (){
            auth()->isAdmin(true);
            Phpfox_Module::instance()->dispatch('greeting.admincp.list');
            return 'controller';
        });
    });
    route('/admincp/delete-greeting', 'greeting.admincp.list');
    route('/greeting/admincp/add-greeting', 'greeting.admincp.add-greeting');