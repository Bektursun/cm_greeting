<?php
namespace Apps\CM_Greeting\Ajax;

class Ajax extends \Phpfox_Ajax
{
    public function setStatus()
    {
        \Phpfox::isAdmin(true);
        \Phpfox::getService('greeting.browse')->setStatus($this->get('id'), $this->get('active'));
        return true;
    }
}