<?php
namespace Apps\CM_Greeting\Controller\Admin;

use Phpfox_Component;

defined('PHPFOX') or exit('NO DICE!');

class ListGreetingController extends Phpfox_Component {
    public function process() {
        \Phpfox::isAdmin();
        $template = $this->template();
        $template->setTitle('List Greeting');



        $oBrowseService = \Phpfox::getService('greeting.browse');
        $aDelete = $this->request()->get('delete');

        if (!empty($aDelete)) {
            $oBrowseService->delete($aDelete);
            $this->url()->send('admincp.app', ['id' => 'CM_Greeting']);
        }
        $template
            ->assign('aGreets', $oBrowseService->all());
    }
}