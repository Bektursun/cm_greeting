<?php
namespace Apps\CM_Greeting\Controller\Admin;

class AddController extends \Phpfox_Component
{
    public function process()
    {
        \Phpfox::isAdmin();
        $template = $this->template();
        $template->setTitle('Add Greeting');

        $template->setBreadCrumb(_p('Add Greeting'),
            $this->url()->makeUrl('greeting.admincp.add-greeting'));

        $aLanguages = \Language_Service_Language::instance()->getAll();

        foreach ($aLanguages as $aLanguage){
            $aValidation['text' . $aLanguage['language_id']] = _p('provide_greeting') . ' ('. $aLanguage['title'] . ')';
        }

        $bIsEdit = false;

        if ($iId = $this->request()->get('id'))
        {
            if(($aListing = \Phpfox::getService('greeting.browse')->getById($iId)))
            {
                $bIsEdit = true;
                $this->template()->assign(array(
                    'aForms' => $aListing
                ));
            }
        }
        else
            {
                $this->template()->assign('aForms',array
                (
                    'start' => '00.00.00',
                    'end'   => '00.00.00'
                ));
            }
            $aValidation = array(
                'start' => _p('please_enter_the_start_time'),
                'end'   => _p('please_enter_the_end_time'),
            );
        $oValidator = \Phpfox_Validator::instance()->set(array(
           'sFormName' => 'js_greeting_form',
            'aParams'  => $aValidation
        ));

        if ($aVals = $this->request()->get('val'))
        {
            if ($oValidator->isValid($aVals))
            {
                if ($bIsEdit)
                {
                    \Phpfox::getService('greeting.browse')->update($iId, $aVals);
                    $this->url()->send('admincp.app', ['id' => 'CM_Greeting']);
                }
                else
                {
                    if (\Phpfox_Error::isPassed())
                    {
                        \Phpfox::getService('greeting.process')->add($aVals);
                    }
                    $this->url()->send('admincp.app', ['id' => 'CM_Greeting']);
                }
            }
        }

        $this->template()
            ->assign(array(
                'aLanguages' => $aLanguages,
                'bIsEdit' => $bIsEdit,
            ));
            }
}