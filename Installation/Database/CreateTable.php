<?php
namespace Apps\CM_Greeting\Installation\Database;

use Core\App\Install\Database\Table as Table;
use \Core\App\Install\Database\Field as Field;

class CreateTable extends Table {
    /**
     * Set name of this table, can't missing
     */
    protected function setTableName()
    {
        $this->_table_name = 'cm_greeting';
    }

    /**
     * Set all fields of table
     */
    protected function setFieldParams()
    {
        $this->_aFieldParams = [
            'id' => [
                Field::FIELD_PARAM_TYPE           => Field::TYPE_INT,
                Field::FIELD_PARAM_TYPE_VALUE     => 11,
                Field::FIELD_PARAM_OTHER          => 'UNSIGNED NOT NULL',
                Field::FIELD_PARAM_PRIMARY_KEY    => true,
                Field::FIELD_PARAM_AUTO_INCREMENT => true
                    ],
            'start' => [
                Field::FIELD_PARAM_TYPE => Field::TYPE_TIME
                       ],
            'end' => [
	                Field::FIELD_PARAM_TYPE => Field::TYPE_TIME,
			           ],
            'text' => [
                Field::FIELD_PARAM_TYPE => Field::TYPE_VARCHAR,
                Field::FIELD_PARAM_TYPE_VALUE => 64
                      ],
            'is_active' => [
                Field::FIELD_PARAM_TYPE => Field::TYPE_TINYINT,
                Field::FIELD_PARAM_TYPE_VALUE => 1
            ]
        ];
    }
}
