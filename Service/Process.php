<?php
namespace Apps\CM_Greeting\Service;
defined('PHPFOX') or exit('NO DICE!');
class Process extends \Phpfox_Service {
    protected $_sTable = 'cm_greeting';

    public function update($iId, $aVals) {
        $aLanguages = \Language_Service_Language::instance()->getAll();
        if (\Core\Lib::phrase()->isPhrase($aVals['text'])) {
            $finalPhrase = $aVals['text'];

            foreach ($aLanguages as $aLanguage) {
                if (isset($aVals['text' . $aLanguage['language_id']])){
                    $text = $aVals['text' . $aLanguage['language_id']];
                    \Language_Service_Language::instance()->updateVarName($aLanguage['language_id'], $aVals['text'], $text);
                }
            }
        } else {
                $text = $aVals['text' . $aLanguages[0]['language_id']];
                $phrase_var_name = 'cm_greeting_' . md5('CM Greeting' . $text . PHPFOX_TIME);
        }
    }

    public function add($aVals)
    {
        $iSize = 128;
        $oFile = \Phpfox_File::instance();
        $mLoaded = null;
        $oImage = \Phpfox_Image::instance();
        $bIsEdit = isset($aVals['photo_id']) && $aVals['photo_id'] > 0;


        $sVarName = 'greeting_' . uniqid();
        foreach ($aVals['text'] as  $sLang => $sText) {
            \Phpfox::getService('language.phrase.process')->add([
                'var_name' => $sVarName,
                'module' => 'greeting|greeting',
                'product_id' => 'phpfox',
                'text' => [$sLang => $sText],
            ]);
        }

        $this->database()->insert(\Phpfox::getT($this->_sTable), [

                    'start'     => $aVals['start'],
                    'end'       => $aVals['end'],
                    'text'      => $sVarName,
                    'is_active' => 1
        ]);
    }
}