<?php
namespace Apps\CM_Greeting\Service;

class Browse extends \Phpfox_Service {
    public function __construct() {
        \Phpfox::isAdmin();
    }
    protected $_sTable = 'cm_greeting';

    public function all(){
       return $this->database()
            -> select("*")
            -> from(\Phpfox::getT($this->_sTable))
            -> executeRows();
    }

    public function delete($iId)
    {
        $this->database()->delete(\Phpfox::getT('cm_greeting'), 'id = ' . (int) $iId);

    }

    public function setStatus($iId, $iStatus)
    {
        $iId = (int) $iId;
        return $this->database()->update(\Phpfox::getT($this->_sTable), ['`is_active`' => $iStatus], '`id` = ' . $iId);
    }

    public function update($iId, $aVals)
    {
        $this->database()
            ->update(\Phpfox::getT($this->_sTable), [
            'start'     => $aVals['start'],
            'end'       => $aVals['end'],
        ], 'id=' . (int) $iId);
    }

    public function getById($iId)
    {
        return $this->database()->select('*')
            ->from(\Phpfox::getT($this->_sTable))
            ->where('id =' . $iId)
            ->executeRow();
    }

    public function timeComparsion($sTime)
    {
        $sTime = date('h:m:s',$sTime);
        return
            $this->database()->select("*")
            ->from(\Phpfox::getT($this->_sTable))
            ->where(' `start` < \'' . $sTime . '\' AND `end` > \'' . $sTime . '\'')
            ->executeRow();
    }
}