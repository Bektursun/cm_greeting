<?php
namespace Apps\CM_Greeting\Block;


class GreetingBlock extends \Phpfox_Component
{
    public function process()
    {
        $sTime = \Phpfox::getTime();
        $aGreeting = \Phpfox::getService('greeting.browse')->timeComparsion($sTime);

        if (empty($aGreeting)){
            return false;
        }
        else
        {
            $this->template()->assign
            ([
                'aVal'  => $aGreeting,
            ]);
        }

        $sShowedMessages = \Phpfox::getCookie('greeting_messages');
        $aShowedMessages = empty($sShowedMessages) ? [] : json_decode($sShowedMessages, true);

        if (isset($aShowedMessages[$aGreeting['id']]))
        {
            return false;
        }

        $aShowedMessages[$aGreeting['id']] = $aGreeting['id'];
        $iLifetime =  strtotime('tomorrow') - $sTime;
        \Phpfox::setCookie('greeting_messages', json_encode($aShowedMessages), $iLifetime);

        $currentUserName = \Phpfox::getUserBy('full_name');


        $this->template()->assign(
            [
                'userName' => $currentUserName,
            ]);

        return 'block';
    }
}
